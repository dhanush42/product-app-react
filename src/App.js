import React from 'react';
import ProductManagement from './products-service/ProductManagement';

function App() {
    return (
        <div>
            <h1>Welcome to My Product App</h1>
            <ProductManagement />
        </div>
    );
}

export default App;
