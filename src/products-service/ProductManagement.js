import React, { useState, useEffect } from 'react';
import './ProductManagement.css';

function ProductManagement() {
    const [products, setProducts] = useState([]);
    const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0 });
    const [editMode, setEditMode] = useState(null);
    const [editProduct, setEditProduct] = useState({ id: null, name: '', description: '', price: 0 });
    const [errors, setErrors] = useState({});

    useEffect(() => {
        fetchProducts();
    }, []);

    const fetchProducts = async () => {
        try {
            const response = await fetch('https://localhost:44330/api/products');
            const data = await response.json();
            setProducts(data);
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setNewProduct({ ...newProduct, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        // Validate form inputs
        const validationErrors = {};
        if (!newProduct.name.trim()) {
            validationErrors.name = 'Name is required';
        }
        if (!newProduct.description.trim()) {
            validationErrors.description = 'Description is required';
        }
        if (!newProduct.price || isNaN(newProduct.price) || newProduct.price <= 0) {
            validationErrors.price = 'Price must be a positive number';
        }

        if (Object.keys(validationErrors).length > 0) {
            setErrors(validationErrors);
            return;
        }

        try {
            const response = await fetch('https://localhost:44330/api/products', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(newProduct),
            });
            if (response.ok) {
                const product = await response.json();
                setProducts([...products, product]);
                setNewProduct({ name: '', description: '', price: 0 });
                setErrors({});
            } else {
                console.error('Failed to add product');
            }
        } catch (error) {
            console.error('Error adding product:', error);
        }
    };

    const handleEditInputChange = (e) => {
        const { name, value } = e.target;
        setEditProduct({ ...editProduct, [name]: value });
    };

    const handleEdit = (product) => {
        setEditMode(product.id);
        setEditProduct({ ...product });
    };

    const handleUpdate = async () => {
        try {
            const response = await fetch(`https://localhost:44330/api/products/${editProduct.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(editProduct),
            });
            if (response.ok) {
                setProducts(products.map((p) => (p.id === editProduct.id ? editProduct : p)));
                setEditMode(null);
                setEditProduct({ id: null, name: '', description: '', price: 0 });
            } else {
                console.error('Failed to update product');
            }
        } catch (error) {
            console.error('Error updating product:', error);
        }
    };

    const handleDelete = async (id) => {
        try {
            const response = await fetch(`https://localhost:44330/api/products/${id}`, {
                method: 'DELETE',
            });
            if (response.ok) {
                setProducts(products.filter(product => product.id !== id));
            } else {
                console.error('Failed to delete product');
            }
        } catch (error) {
            console.error('Error deleting product:', error);
        }
    };

    return (
        <div>
            <h1>Product Management</h1>
            <div className="container">
    <form onSubmit={handleSubmit}>
        <div className="input-container">
            <input
                type="text"
                name="name"
                placeholder="Product Name"
                value={newProduct.name}
                onChange={handleInputChange}
            />
            {errors.name && <div className="error-message">{errors.name}</div>}
        </div>
        <div className="input-container">
            <input
                type="text"
                name="description"
                placeholder="Product Description"
                value={newProduct.description}
                onChange={handleInputChange}
            />
            {errors.description && <div className="error-message">{errors.description}</div>}
        </div>
        <div className="input-container">
            <input
                type="number"
                name="price"
                placeholder="Product Price"
                value={newProduct.price}
                onChange={handleInputChange}
            />
            {errors.price && <div className="error-message">{errors.price}</div>}
        </div>
        <button type="submit">Add Product</button>
    </form>
</div>

            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map((product) => (
                        <tr key={product.id}>
                            <td>{editMode === product.id ? (
                                <input
                                    type="text"
                                    name="name"
                                    value={editProduct.name}
                                    onChange={handleEditInputChange}
                                />
                            ) : product.name}</td>
                            <td>{editMode === product.id ? (
                                <input
                                    type="text"
                                    name="description"
                                    value={editProduct.description}
                                    onChange={handleEditInputChange}
                                />
                            ) : product.description}</td>
                            <td>{editMode === product.id ? (
                                <input
                                    type="number"
                                    name="price"
                                    value={editProduct.price}
                                    onChange={handleEditInputChange}
                                />
                            ) : product.price}</td>
                            <td>
                                {editMode === product.id ? (
                                    <>
                                        <button onClick={handleUpdate}>Update</button>
                                        <button onClick={() => setEditMode(null)}>Cancel</button>
                                    </>
                                ) : (
                                    <>
                                        <button onClick={() => handleEdit(product)}>Edit</button>
                                        <button onClick={() => handleDelete(product.id)}>Delete</button>
                                    </>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ProductManagement;
